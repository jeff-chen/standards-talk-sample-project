import sys, re
import word_counts


def main(file_name):
  '''
  To run: python word_length.py ___.txt
  and a file called ___.csv is created
  '''
  f = open(file_name, 'r')
  output_file_name = file_name.replace(".txt",".csv")
  text = f.read()
  my_dict = word_counts.histogram(text)
  print(my_dict)
  print(word_counts.histogram_to_csv(my_dict))
  f2 = open(output_file_name, 'w')
  f2.write(word_counts.histogram_to_csv(my_dict))
  
main(sys.argv[1])