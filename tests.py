import unittest
import word_counts
#see: https://docs.python.org/2/library/unittest.html

class TestStringMethods(unittest.TestCase):
  
   def test_histogram(self):
     self.assertEqual(word_counts.histogram("Hi world"),{2:1, 5:1})
     
   def test_punctuation(self):
     self.assertEqual(word_counts.histogram("Hi world!!"),{2:1, 5:1})
   def test_csv(self):
     test_output_str = "length,freq\n2,1\n5,1\n"
     self.assertEqual(word_counts.histogram_to_csv({2:1, 5:1}), test_output_str)
  
if __name__ == '__main__':
    unittest.main()
