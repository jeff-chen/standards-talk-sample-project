#run with Rscript word_compare.R arg1 arg2
library(ggplot2)

unpackHistogram <- function(input_hist){
  #takes in a df of length first followed by frequency
  #and returns something like 
  #eg 5,2 and 4,3 becomes (5,5,4,4,4)
  output_counts = c()
  for(i in 1:dim(input_hist)[1]){
    output_counts <- c(output_counts, rep(input_hist[i,1],input_hist[i,2]))
  }
  return(unlist(output_counts))
}
args <- commandArgs(TRUE)

if(length(args) == 2){
  file1 <- args[1]
  file2 <- args[2]
  left_df <- read.csv(file1)
  right_df <- read.csv(file2)
  left_df$group = gsub('.csv', '',file1)
  right_df$group = gsub('.csv','',file2)
  total_df <- rbind(left_df, right_df)
  
  print("Doing t-test of both groups:")
  counts1 <- unpackHistogram(left_df)
  counts2 <- unpackHistogram(right_df)
  pval <- t.test(counts1, counts2)$p.val
  cat("P value is : ", pval, "\n")
  cat("Average word length in file ", file1, ": ", mean(counts1), "\n")
  cat("Average word length in file ", file2, ": ", mean(counts2), "\n")
  write.table(pval,file='pvalue.txt')
  print("Saving to plot plot.pdf:")
  ggplot(total_df, aes(x=factor(length), y=freq,fill=factor(group))) + geom_bar(stat="identity",position="dodge")
  ggsave('plot.pdf', plot = last_plot())
} else {
  print("run with Rscript word_compare.R file1.csv file2.csv")
}