import re

def histogram(text):
  '''
  input: A bunch of text such as "The quick brown fox jumped over the lazy dog."
  output: A dict of counts sorted by word length. Key = length of word, value= frequency e.g. {3:4, 4:2, 5:2, 6:1}
  '''
  final_dict = {}
  words = re.split('\s+', text)#text.split(' ')
  words = map(lambda foo: foo.strip(',.?!;'), words)
  for word in words:
    word_length = len(word)
    if final_dict.get(word_length):
      final_dict[word_length] += 1
    else:
      final_dict[word_length] = 1
  return(final_dict)
  
def histogram_to_csv(input_dict):
  '''
  input: a dict where both key and value are integers, e.g. {3:4, 4:2}
  output: a text string, in csv format, showing the keys as first column and values as second column, with ehaders
  e.g. length,freq
       3,4
       4,2
  '''
  final_string = 'length,freq\n'
  max_length = max(input_dict.keys())
  for i in range(max_length + 1):
    if(input_dict.get(i)):
      final_string += str(i) + "," + str(input_dict[i]) + "\n"
  return(final_string)